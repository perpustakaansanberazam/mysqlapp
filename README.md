# mysqlapp

Terdapat beberapa modifikasi pada bagian mysql app. Diantaranya pada tabel **customers** ditambahkan kolom **is_admin** dan **password**. Lalu pada tabel **borrow** ditambahkan kolom **email** <br>
Ketika melakukan akses token, maka user harus memberikan parameter email dan password. Lalu tidak semua routing dapat diakses oleh user biasa, terdapat beberapa route yang hanya dapat diakses oleh admin saja. <br>
Selain itu, pada mysqlapp ini menggunakan ORM Flask_SQLAlchemy <br> <br>

<h1> Customers Route </h1> <br>

- @app.route("/users", methods=["GET"]) <br>
tanpa perlu menambahkan parameter namun diperlukan akses token dan juga role sebagai admin. Contoh penggunaan endpoint :

http://127.0.0.1:5000/users

- @app.route("/user", methods=["POST"])) <br>
tanpa perlu akses token maupun role admin. Menggunakan parameter berupa userid. Contoh akses endpoint :

http://127.0.0.1:5000/user

Dengan contoh parameter :

{
    "userid":1
}

- @app.route("/user/tokenrequest", methods=["POST"]) <br>
parameter
{
    "email": "azamfuadi@mail.com",
    "password": "apahayoo"
}

- @app.route("/user/insert", methods=["POST"]) <br>

{
    "username":"azamfuadi",
    "password":"apahayoo",
    "is_admin":1,
    "namadepan":"muhamad",
    "namabelakang":"azamfuadi",
    "email":"azamfuadi@mail.com"
}

- @app.route("/user/update", methods=["POST"])

{
    "username": "azam",
    "password": "apahayoo",
    "namadepan": "azamfuadi",
    "namabelakang": "mantab",
    "email": "azam@mail.com"
}

- @app.route("/user/changerole", methods=["POST"])

{
    "userid": 2,
    "id_admin": "1"
}

- @app.route("/user/delete", methods=["POST"])

{
    "userid": 2,
    "id_admin": "1"
}


<h1> Borrows Route </h1> <br>

- @app.route("/borrows", methods=["GET"])

- @app.route("/borrow", methods=["GET"])

{
    "borrowid":3
}

- @app.route("/borrowsbyuserid", methods=["GET"])

{
    "userid":1
}

- @app.route("/borrowsbycurrentall", methods=["GET"])

- @app.route("/borrowsbycurrentactive", methods=["GET"])

- @app.route("/borrow/insertcurrent", methods=["POST"])

{
    "bookid":"606f04cfb65629dd95784782"
}

- @app.route("/borrow/insertuserid", methods=["POST"])

{
    "userid":2,
    "bookid":"606f04cfb65629dd95784784"
}

- @app.route("/borrow/changestatus", methods=["POST"])
