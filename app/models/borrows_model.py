from mysql.connector import connect
from sqlalchemy import MetaData, Table
from sqlalchemy import Column, Integer, String, SmallInteger, DateTime, ForeignKey
from app import mysql_engine, Base

# untuk table buat manual


class Borrows(Base):
    __tablename__ = 'borrows'
    borrowid = Column(Integer, primary_key=True)
    borrowdate = Column(DateTime)
    userid = Column('userid', Integer, ForeignKey('customers.userid'))
    bookid = Column(String)
    bookname = Column(String)
    is_active = Column(SmallInteger)
    email = Column((String))
