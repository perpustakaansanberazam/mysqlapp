from mysql.connector import connect
from sqlalchemy import MetaData, Table
from sqlalchemy import Column, Integer, String, SmallInteger
from app import mysql_engine, Base

metadata = MetaData()
Customers = Table('customers', metadata,
                  Column('userid', Integer, primary_key=True, nullable=False),
                  Column('username', String(50), nullable=False),
                  Column('password', String(100), nullable=False),
                  Column('is_admin', SmallInteger, nullable=False),
                  Column('namadepan', String(50), nullable=False),
                  Column('namabelakang', String(50), nullable=True),
                  Column('email', String(50), nullable=False),
                  )
Customers.create(mysql_engine, checkfirst=True)


class Customers(Base):
    __tablename__ = 'customers'
    userid = Column(Integer, primary_key=True)
    username = Column(String)
    password = Column(String)
    is_admin = Column(String)
    namadepan = Column(String)
    namabelakang = Column(String)
    email = Column(String)
