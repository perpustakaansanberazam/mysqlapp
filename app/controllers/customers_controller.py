from flask import jsonify, request
from app import session
from app.models.customers_model import Customers
from flask_jwt_extended import *
from passlib.hash import sha256_crypt
import datetime
import json


@jwt_required()
def showAllUsers():
    current = get_jwt_identity()
    if current['is_admin'] == 1:
        users = session.query(Customers).all()
        result = []
        for items in users:
            user = {
                "userid": items.userid,
                "username": items.username,
                "password": items.password,
                "is_admin": items.is_admin,
                "firstname": items.namadepan,
                "lastname": items.namabelakang,
                "email": items.email,
            }
            result.append(user)
        return jsonify(result)
    else:
        return jsonify({"message": "Admin Access Only"})


def showUserById(**param):
    dbresult = session.query(Customers).filter(
        Customers.userid == param['userid']).one()
    user = {
        "userid": dbresult.userid,
        "username": dbresult.username,
        "password": dbresult.password,
        "is_admin": dbresult.is_admin,
        "firstname": dbresult.namadepan,
        "lastname": dbresult.namabelakang,
        "email": dbresult.email,
    }
    return jsonify(user)


def generateToken(**param):
    dbresult = session.query(Customers).filter(
        Customers.email == param['email']).one()
    if dbresult is not None:
        authenticated = sha256_crypt.verify(
            param['password'], dbresult.password)
        if authenticated:
            user = {
                "userid": dbresult.userid,
                "username": dbresult.username,
                "email": dbresult.email,
                "is_admin": dbresult.is_admin
            }
            expires = datetime.timedelta(days=1)
            expires_refresh = datetime.timedelta(days=3)
            access_token = create_access_token(
                user, fresh=True, expires_delta=expires)

            data = {
                "data": user,
                "token_access": access_token
            }
        else:
            data = {
                "message": "Password salah"
            }
    else:
        data = {
            "message": "Email tidak terdaftar"
        }
    return jsonify(data)


def insertUser(**params):
    newUser = Customers(
        username=params['username'],
        password=sha256_crypt.encrypt(params['password']),
        is_admin=params['is_admin'],
        namadepan=params['namadepan'],
        namabelakang=params['namabelakang'],
        email=params['email'],
    )
    session.add(newUser)
    session.commit()
    return jsonify({"message": "Insert User Succes"})


@jwt_required()
def updateUser(**params):
    current = get_jwt_identity()
    session.query(Customers).filter(
        Customers.userid == current['userid']).update({
            "username": params['username'],
            "password": sha256_crypt.encrypt(params['password']),
            "namadepan": params['namadepan'],
            "namabelakang": params['namabelakang'],
            "email": params['email'],
        })
    session.commit()
    return jsonify({"message": "Update User Succes"})


@jwt_required()
def changeRole(**params):
    current = get_jwt_identity()
    if current['is_admin'] == 1:
        session.query(Customers).filter(
            Customers.userid == params['userid']).update({
                "is_admin": params['isadmin']
            })
        return jsonify({"message": "Change Role Succes"})
    else:
        return jsonify({"message": "Admin Access Only"})


@jwt_required()
def deleteUser(**params):
    current = get_jwt_identity()
    if current['is_admin'] == 1:
        session.query(Customers).filter(
            Customers.userid == params['userid']).delete()
        return jsonify({"message": "Delete Succes"})
    else:
        return jsonify({"message": "Admin Access Only"})
