from app import session
from app.models.borrows_model import Borrows
from app.models.customers_model import Customers
from flask import jsonify, request
from flask_jwt_extended import *
import json
import datetime
import requests


@jwt_required()
def showAllBorrows():
    current = get_jwt_identity()
    if current['is_admin'] == 1:
        borrows = session.query(Borrows).all()
        result = []
        for items in borrows:
            borrow = {
                "borrowid": items.borrowid,
                "borrowdate": items.borrowdate,
                "userid": items.userid,
                "bookid": items.bookid,
                "bookname": items.bookname,
                "is_active": items.is_active,
                "email": items.email
            }
            result.append(borrow)
        return jsonify(result)
    else:
        return jsonify({"message": "Admin Access Only"})


@jwt_required()
def showById(**params):
    dbresult = session.query(Borrows, Customers).join(Customers).filter(
        Borrows.borrowid == params['borrowid']).one()
    result = []
    if dbresult is not None:
        bookid = json.dumps({"id": dbresult.Borrows.bookid})
        bookDetail = getBookById(bookid)
        borrow = {
            "username": dbresult.Customers.username,
            "email": dbresult.Customers.email,
            "borrowid": dbresult.Borrows.borrowid,
            "borrowdate": dbresult.Borrows.borrowdate,
            "bookid": dbresult.Borrows.bookid,
            "bookname": dbresult.Borrows.bookname,
            "author": bookDetail["pengarang"],
            "releaseyear": bookDetail["tahunterbit"],
            "genre": bookDetail["genre"]
        }
        result.append(borrow)
    else:
        result = dbresult
    return jsonify(result)


@jwt_required()
def showByUserid(**params):
    current = get_jwt_identity()
    if current['is_admin'] == 1:
        dbresult = session.query(Borrows, Customers).join(Customers).filter(
            Borrows.userid == params['userid']).all()
        result = []
        if dbresult is not None:
            for item in dbresult:
                bookid = json.dumps({"id": item.Borrows.bookid})
                bookDetail = getBookById(bookid)
                borrow = {
                    "username": item.Customers.username,
                    "email": item.Customers.email,
                    "borrowid": item.Borrows.borrowid,
                    "borrowdate": item.Borrows.borrowdate,
                    "bookid": item.Borrows.bookid,
                    "bookname": item.Borrows.bookname,
                    "author": bookDetail["pengarang"],
                    "releaseyear": bookDetail["tahunterbit"],
                    "genre": bookDetail["genre"]
                }
                result.append(borrow)
        else:
            result = dbresult
        return jsonify(result)
    else:
        return jsonify({"message": "Admin Access Only"})


@jwt_required()
def showByCurrentEmailAll():
    current = get_jwt_identity()
    dbresult = session.query(Borrows, Customers).join(Customers).filter(
        Borrows.email == current['email']).all()
    result = []
    if dbresult is not None:
        for item in dbresult:
            bookid = json.dumps({"id": item.Borrows.bookid})
            bookDetail = getBookById(bookid)
            borrow = {
                "username": item.Customers.username,
                "email": item.Customers.email,
                "borrowid": item.Borrows.borrowid,
                "borrowdate": item.Borrows.borrowdate,
                "bookid": item.Borrows.bookid,
                "bookname": item.Borrows.bookname,
                "author": bookDetail["pengarang"],
                "releaseyear": bookDetail["tahunterbit"],
                "genre": bookDetail["genre"]
            }
            result.append(borrow)
    else:
        result = dbresult
    return jsonify(result)


@jwt_required()
def showByCurrentEmailActive():
    current = get_jwt_identity()
    dbresult = session.query(Borrows, Customers).join(Customers).filter(
        Borrows.email == current['email'], Borrows.is_active == 1).all()
    result = []
    if dbresult is not None:
        for item in dbresult:
            bookid = json.dumps({"id": item.Borrows.bookid})
            bookDetail = getBookById(bookid)
            borrow = {
                "username": item.Customers.username,
                "email": item.Customers.email,
                "borrowid": item.Borrows.borrowid,
                "borrowdate": item.Borrows.borrowdate,
                "bookid": item.Borrows.bookid,
                "bookname": item.Borrows.bookname,
                "author": bookDetail["pengarang"],
                "releaseyear": bookDetail["tahunterbit"],
                "genre": bookDetail["genre"]
            }
            result.append(borrow)
    else:
        result = dbresult
    return jsonify(result)


@jwt_required()
def insertByCurrent(**params):
    current = get_jwt_identity()
    borrowdate = datetime.datetime.now().isoformat()
    newbookid = json.dumps({"id": params["bookid"]})
    book = getBookById(newbookid)
    newBorrow = Borrows(
        borrowdate=borrowdate,
        userid=current['userid'],
        bookid=params["bookid"],
        bookname=book["nama"],
        is_active=1,
        email=current['email'],
    )
    session.add(newBorrow)
    session.commit()
    return jsonify({"message": "Insert Borrow Succes"})


@jwt_required()
def insertByUserid(**params):
    current = get_jwt_identity()
    if current['is_admin'] == 1:
        customer = session.query(Customers).filter(
            Customers.userid == params['userid']).one()
        borrowdate = datetime.datetime.now().isoformat()
        newbookid = json.dumps({"id": params['bookid']})
        book = getBookById(newbookid)
        newBorrow = Borrows(
            borrowdate=borrowdate,
            userid=customer.userid,
            bookid=params["bookid"],
            bookname=book["nama"],
            is_active=1,
            email=customer.email,
        )
        session.add(newBorrow)
        session.commit()
        return jsonify({"message": "Insert Borrow Succes"})
    else:
        return jsonify({"message": "Admin Access Only"})


@jwt_required()
def changeStatus(**params):
    current = get_jwt_identity()
    if current['is_admin'] == 1:
        session.query(Borrows).filter(
            Borrows.borrowid == params['borrowid']).update({
                "is_active": params['isactive']
            })
        return jsonify({"message": "Change Status Succes"})
    else:
        return jsonify({"message": "Admin Access Only"})


def getBookById(data):
    bookdata = requests.get(url="http://localhost:8000/bookbyid", data=data)
    return bookdata.json()
