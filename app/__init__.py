from flask import Flask
from config import Config
from flask_jwt_extended import JWTManager
from flask_sqlalchemy_session import flask_scoped_session
from sqlalchemy import create_engine, MetaData, Table
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

engine = create_engine(
    'mysql+mysqlconnector://root:@localhost:3306', echo=True)
existing_databases = engine.execute("SHOW DATABASES;")
# Results are a list of single item tuples, so unpack each tuple
existing_databases = [d[0] for d in existing_databases]

# Create database if not exists
if 'perpustakaan' not in existing_databases:
    engine.execute("CREATE DATABASE perpustakaan")
    print("Created database perpustakaan")

Base = declarative_base()
url = 'mysql+mysqlconnector://root:@localhost:3306/perpustakaan'
mysql_engine = create_engine(url, echo=True)
session_factory = sessionmaker(bind=mysql_engine)
    
app = Flask(__name__)
app.config.from_object(Config)
session = flask_scoped_session(session_factory, app)
jwt = JWTManager(app)

from app.routers.customers_router import *
from app.routers.borrows_router import *

app.register_blueprint(customers_blueprint)
app.register_blueprint(borrows_blueprint)
