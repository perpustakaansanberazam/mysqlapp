from app import app
from app.controllers import borrows_controller
from flask import Blueprint, request

borrows_blueprint = Blueprint("borrows_router", __name__)


@app.route("/borrows", methods=["GET"])
def showBorrows():
    return borrows_controller.showAllBorrows()


@app.route("/borrow", methods=["GET"])
def showBorrow():
    params = request.json
    return borrows_controller.showById(**params)


@app.route("/borrowsbyuserid", methods=["GET"])
def showBorrowUserid():
    params = request.json
    return borrows_controller.showByUserid(**params)


@app.route("/borrowsbycurrentall", methods=["GET"])
def showBorrowByCurrentAll():
    return borrows_controller.showByCurrentEmailAll()


@app.route("/borrowsbycurrentactive", methods=["GET"])
def showBorrowByCurrentActive():
    return borrows_controller.showByCurrentEmailActive()


@app.route("/borrow/insertcurrent", methods=["POST"])
def addDataCurrent():
    params = request.json
    return borrows_controller.insertByCurrent(**params)


@app.route("/borrow/insertuserid", methods=["POST"])
def addDataUserid():
    params = request.json
    return borrows_controller.insertByUserid(**params)


@app.route("/borrow/changestatus", methods=["POST"])
def changeStatus():
    params = request.json
    return borrows_controller.changeStatus(**params)
