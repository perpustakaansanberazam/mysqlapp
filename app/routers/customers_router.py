from app import app
from app.controllers import customers_controller
from flask import Blueprint, request

customers_blueprint = Blueprint("customers_router", __name__)


@app.route("/")
def main():
    return "Welcome"


@app.route("/users", methods=["GET"])
def showAllUsers():
    return customers_controller.showAllUsers()


@app.route("/user", methods=["POST"])
def showUserById():
    param = request.json
    return customers_controller.showUserById(**param)


@app.route("/user/tokenrequest", methods=["POST"])
def requestToken():
    params = request.json
    return customers_controller.generateToken(**params)


@app.route("/user/insert", methods=["POST"])
def tambahData():
    params = request.json
    return customers_controller.insertUser(**params)


@app.route("/user/update", methods=["POST"])
def ubahData():
    params = request.json
    return customers_controller.updateUser(**params)


@app.route("/user/changerole", methods=["POST"])
def ubahRole():
    params = request.json
    return customers_controller.changeRole(**params)


@app.route("/user/delete", methods=["POST"])
def hapusData():
    params = request.json
    return customers_controller.deleteUser(**params)
